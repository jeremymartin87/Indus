<?php
try{
    $pdo = new PDO('sqlite:'.'Crud/db_Test.sqlite');
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // ERRMODE_WARNING | ERRMODE_EXCEPTION | ERRMODE_SILENT
} catch(Exception $e) {
    echo "Impossible d'accéder à la base de données SQLite : ".$e->getMessage();
    die();
}

$pdo->query("CREATE TABLE IF NOT EXISTS contact (
    id            INTEGER         PRIMARY KEY AUTOINCREMENT,
    nom         VARCHAR( 50 ),
    prenom       VARCHAR( 50 ),
    email  VARCHAR( 50 ),
    telephone VARCHAR( 50 )
);");
?>