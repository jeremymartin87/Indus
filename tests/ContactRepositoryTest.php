<?php

//require(__DIR__ . '/../models/contact_model.php');
//require(__DIR__ . '/../Repositories/ContactRepository.php');

use PHPUnit\Framework\TestCase;

class ContactRepositoryTest extends TestCase {
    private $contactRepository;
    private $db;

    protected function setUp(): void {
        $testDbPath = 'tests/db_Test.sqlite';
        $this->db = new SQLite3($testDbPath);
        $this->contactRepository = new ContactRepository($this->db);

        $this->db->exec("DELETE FROM contact");
    }


    public function testGetAllContacts() {
        $this->contactRepository->ajouterContact(new Contact("Jean", "TRUC", "Jean@TRUC.com", "123-456-7890"));
        $this->contactRepository->ajouterContact(new Contact("Michel", "DUPONT", "Michel@DUPONT.com", "987-654-3210"));

        $contacts = $this->contactRepository->getAllContacts();
        $this->assertCount(2, $contacts);
    }

    protected function tearDown(): void {
        $this->db->close();
    }
}
