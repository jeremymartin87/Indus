<?php

class ContactRepository {
    private $db;

    public function __construct() {
        try {
            $dbPath = 'tests/db_Test.sqlite';
            $this->db = new SQLite3($dbPath);

            $this->initDatabase();
        } catch (Exception $e) {
            echo "Error connecting to the database: " . $e->getMessage();
        }
    }


    private function initDatabase() {
        $query = "CREATE TABLE IF NOT EXISTS contact (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    nom TEXT NOT NULL,
                    prenom TEXT NOT NULL,
                    email TEXT NOT NULL,
                    telephone TEXT NOT NULL
                )";

        $this->db->exec($query);
    }

    public function ajouterContact(Contact $contact) {
        try {
            $query = "INSERT INTO contact (nom, prenom, email, telephone) VALUES (:nom, :prenom, :email, :telephone)";
            $stmt = $this->db->prepare($query);

            $stmt->bindParam(':nom', $contact->nom, SQLITE3_TEXT);
            $stmt->bindParam(':prenom', $contact->prenom, SQLITE3_TEXT);
            $stmt->bindParam(':email', $contact->email, SQLITE3_TEXT);
            $stmt->bindParam(':telephone', $contact->telephone, SQLITE3_TEXT);

            $stmt->execute();
        } catch (Exception $e) {
            echo "Error adding contact: " . $e->getMessage();
        }
    }

    public function getAllContacts() {
        try {
            $query = "SELECT * FROM contact";
            $result = $this->db->query($query);

            $contacts = [];
            while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
                $contacts[] = new Contact($row['nom'], $row['prenom'], $row['email'], $row['telephone']);
            }

            return $contacts;
        } catch (Exception $e) {
            echo "Error getting all contacts: " . $e->getMessage();
            return [];
        }
    }


    public function getContactById($id) {
        try {
            $query = "SELECT * FROM contact WHERE id = :id";
            $stmt = $this->db->prepare($query);
            $stmt->bindParam(':id', $id, SQLITE3_INTEGER);
            $result = $stmt->execute();

            $row = $result->fetchArray(SQLITE3_ASSOC);

            if ($row) {
                return new Contact($row['nom'], $row['prenom'], $row['email'], $row['telephone']);
            } else {
                return null;
            }
        } catch (Exception $e) {
            echo "Error getting contact by ID: " . $e->getMessage();
            return null;
        }
    }

    public function mettreAJourContact(Contact $contact) {
        try {
            $query = "UPDATE contact SET nom = :nom, prenom = :prenom, email = :email, telephone = :telephone WHERE id = :id";
            $stmt = $this->db->prepare($query);

            $stmt->bindParam(':id', $contact->id, SQLITE3_INTEGER);
            $stmt->bindParam(':nom', $contact->nom, SQLITE3_TEXT);
            $stmt->bindParam(':prenom', $contact->prenom, SQLITE3_TEXT);
            $stmt->bindParam(':email', $contact->email, SQLITE3_TEXT);
            $stmt->bindParam(':telephone', $contact->telephone, SQLITE3_TEXT);

            $stmt->execute();
        } catch (Exception $e) {
            echo "Error updating contact: " . $e->getMessage();
        }
    }

    public function supprimerContact($id) {
        try {
            $query = "DELETE FROM contact WHERE id = :id";
            $stmt = $this->db->prepare($query);
            $stmt->bindParam(':id', $id, SQLITE3_INTEGER);
            $stmt->execute();
        } catch (Exception $e) {
            echo "Error deleting contact: " . $e->getMessage();
        }
    }

}

$dbPath = 'tests/db_Test.sqlite';
$contactRepository = new ContactRepository($dbPath);

try {
    $dbPath = 'tests/db_Test.sqlite';
    $contactRepository = new ContactRepository($dbPath);
} catch (Exception $e) {
    echo "Error creating ContactRepository: " . $e->getMessage();
}

?>
