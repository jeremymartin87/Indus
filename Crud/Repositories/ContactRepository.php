<?php
namespace Indus\Crud\Repositories;

class ContactRepository {
    private $db;

    public function __construct() {
        $dbPath = 'tests/db_Test.sqlite';
        $this->db = new SQLite3($dbPath);

        $this->initDatabase();
    }


    private function initDatabase() {
        $query = "CREATE TABLE IF NOT EXISTS contact (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    nom TEXT NOT NULL,
                    prenom TEXT NOT NULL,
                    email TEXT NOT NULL,
                    telephone TEXT NOT NULL
                )";

        $this->db->exec($query);
    }

    public function ajouterContact(Contact $contact) {
        $query = "INSERT INTO contact (nom, prenom, email, telephone) VALUES (:nom, :prenom, :email, :telephone)";
        $stmt = $this->db->prepare($query);

        $stmt->bindParam(':nom', $contact->nom, SQLITE3_TEXT);
        $stmt->bindParam(':prenom', $contact->prenom, SQLITE3_TEXT);
        $stmt->bindParam(':email', $contact->email, SQLITE3_TEXT);
        $stmt->bindParam(':telephone', $contact->telephone, SQLITE3_TEXT);

        $stmt->execute();
    }

    public function getAllContacts() {
        $query = "SELECT * FROM contact";
        $result = $this->db->query($query);

        $contacts = [];
        while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
            $contacts[] = new Contact($row['nom'], $row['prenom'], $row['email'], $row['telephone']);
        }

        return $contacts;
    }


    public function getContactById($id) {
        $query = "SELECT * FROM contact WHERE id = :id";
        $stmt = $this->db->prepare($query);
        $stmt->bindParam(':id', $id, SQLITE3_INTEGER);
        $result = $stmt->execute();

        $row = $result->fetchArray(SQLITE3_ASSOC);

        if ($row) {
            return new Contact($row['nom'], $row['prenom'], $row['email'], $row['telephone']);
        } else {
            return null;
        }
    }

    public function mettreAJourContact(Contact $contact) {
        $query = "UPDATE contact SET nom = :nom, prenom = :prenom, email = :email, telephone = :telephone WHERE id = :id";
        $stmt = $this->db->prepare($query);

        $stmt->bindParam(':id', $contact->id, SQLITE3_INTEGER);
        $stmt->bindParam(':nom', $contact->nom, SQLITE3_TEXT);
        $stmt->bindParam(':prenom', $contact->prenom, SQLITE3_TEXT);
        $stmt->bindParam(':email', $contact->email, SQLITE3_TEXT);
        $stmt->bindParam(':telephone', $contact->telephone, SQLITE3_TEXT);

        $stmt->execute();
    }

    public function supprimerContact($id) {
        $query = "DELETE FROM contact WHERE id = :id";
        $stmt = $this->db->prepare($query);
        $stmt->bindParam(':id', $id, SQLITE3_INTEGER);
        $stmt->execute();
    }
}

$dbPath = 'tests/db_Test.sqlite';
$contactRepository = new ContactRepository($dbPath);

?>
