<?php

use PHPUnit\Framework\TestCase;

class ContactTest extends TestCase
{
    private $pdo;

    public function testCreateContact()
    {
        // Votre code pour créer un contact et insérer dans la base de données
        $contact = new Contact($this->pdo);
        $id = $contact->create('MARTIN', 'Jeremy', 'martinje@3il.fr', '123456789');

        $this->assertGreaterThan(0, $id, 'Échec de la création du contact');

        // Vérifier si le contact a été correctement inséré
        $result = $this->pdo->query("SELECT * FROM contact WHERE id = $id");
        $this->assertTrue($result->fetch(PDO::FETCH_ASSOC) !== false, 'Le contact n\'a pas été trouvé dans la base de données');
    }
}

?>