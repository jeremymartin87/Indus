<?php

use PHPUnit\Framework\TestCase;

class ContactRepositoryTest extends TestCase {
    private $contactRepository;
    private $db;

    protected function setUp(): void {
        $testDbPath = 'tests/db_Test.sqlite';
        $this->db = new SQLite3($testDbPath);
        $this->contactRepository = new ContactRepository($this->db);

        $this->db->exec("DELETE FROM contact");
    }

    public function testAjouterContact() {
        $nouveauContact = new Contact("Jean", "MARTIN", "Jean@MARTIN.com", "987-654-3210");
        $this->contactRepository->ajouterContact($nouveauContact);

        $contacts = $this->contactRepository->getAllContacts();
        $this->assertCount(1, $contacts);
    }

    public function testGetAllContacts() {
        $this->contactRepository->ajouterContact(new Contact("John", "Doe", "john@example.com", "123-456-7890"));
        $this->contactRepository->ajouterContact(new Contact("Jane", "Doe", "jane@example.com", "987-654-3210"));

        $contacts = $this->contactRepository->getAllContacts();
        $this->assertCount(2, $contacts);
    }

    public function testGetContactById() {
        $this->contactRepository->ajouterContact(new Contact("John", "Doe", "john@example.com", "123-456-7890"));

        $contacts = $this->contactRepository->getAllContacts();
        $idDuContact = $contacts[0]->id;

        $contact = $this->contactRepository->getContactById($idDuContact);
        $this->assertInstanceOf(Contact::class, $contact);
        $this->assertEquals("John", $contact->nom);
    }

    public function testMettreAJourContact() {

        $this->contactRepository->ajouterContact(new Contact("John", "Doe", "john@example.com", "123-456-7890"));

        $contacts = $this->contactRepository->getAllContacts();
        $idDuContact = $contacts[0]->id;

        $contactMaj = new Contact("Jane", "Doe", "jane@example.com", "987-654-3210");
        $contactMaj->id = $idDuContact;
        $this->contactRepository->mettreAJourContact($contactMaj);

        $contactMaj = $this->contactRepository->getContactById($idDuContact);
        $this->assertEquals("Jane", $contactMaj->nom);
    }

    public function testSupprimerContact() {
        $this->contactRepository->ajouterContact(new Contact("John", "Doe", "john@example.com", "123-456-7890"));

        $contacts = $this->contactRepository->getAllContacts();
        $idDuContact = $contacts[0]->id;

        $this->contactRepository->supprimerContact($idDuContact);

        $contactsApresSuppression = $this->contactRepository->getAllContacts();
        $this->assertCount(0, $contactsApresSuppression);
    }

    protected function tearDown(): void {
        $this->db->close();
    }
}
