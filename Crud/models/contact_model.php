<?php

class Contact {

    private $nom;
    private $prenom;
    private $email;
    private $telephone;

    public function __construct($nom, $prenom, $email, $telephone) {
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->email = $email;
        $this->telephone = $telephone;
    }

    public function getNom() {
        return $this->nom;
    }

    public function setNom($nom) {
        $this->nom = $nom;
    }

    public function getPrenom() {
        return $this->prenom;
    }

    public function setPrenom($prenom) {
        $this->prenom = $prenom;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getTelephone() {
        return $this->telephone;
    }

    public function setTelephone($telephone) {
        $this->telephone = $telephone;
    }
}

$monContact = new Contact("Doe", "John", "john.doe@example.com", "123-456-7890");

echo "Nom: " . $monContact->getNom() . "<br>";
echo "Prénom: " . $monContact->getPrenom() . "<br>";
echo "Email: " . $monContact->getEmail() . "<br>";
echo "Téléphone: " . $monContact->getTelephone() . "<br>";

$monContact->setNom("Smith");
$monContact->setPrenom("Jane");
$monContact->setEmail("jane.smith@example.com");
$monContact->setTelephone("987-654-3210");

echo "<br>Après modification :<br>";
echo "Nom: " . $monContact->getNom() . "<br>";
echo "Prénom: " . $monContact->getPrenom() . "<br>";
echo "Email: " . $monContact->getEmail() . "<br>";
echo "Téléphone: " . $monContact->getTelephone() . "<br>";


?>
